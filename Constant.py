# DQN
LEARNING_RATE = 0.0001

# Neuron Network
INPUT_DIMENSIONS = 11

# Utils
BACKGROUND_IMAGE = "img/bg420.png"
NUMBER_OF_FOODS = 60
PLAY_TIMES = 10
TIME_FRAME_DELAY = 30

INFO_DISPLAY_SIZE = 440
GAME_HEIGHT = 400
GAME_WIDTH = GAME_HEIGHT + INFO_DISPLAY_SIZE
