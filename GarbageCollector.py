import os
import random
from random import randint

import matplotlib.pyplot as plt
import numpy as np
import pygame
import seaborn as sns
from keras.utils import to_categorical
from math import exp

import Constant
from DQN import DQNAgent

# Set options to activate or deactivate the game view, and its speed
display_option = True
speed = 0
pygame.font.init()

x = 50
y = 50

os.environ['SDL_VIDEO_WINDOW_POS'] = "%d,%d" % (x, y)


class Game:

    def __init__(self, game_width, game_height):
        pygame.display.set_caption('GarbageCollector')
        self.game_width = game_width
        self.game_height = game_height
        self.gameDisplay = pygame.display.set_mode((game_width, game_height + 40), 0, 0, 0)
        self.bg = pygame.image.load(Constant.BACKGROUND_IMAGE)
        self.crash = False
        self.player = Player(self)
        self.food = Food()
        self.score = 0

    def create_foods(self):
        for i in range(Constant.NUMBER_OF_FOODS):
            while True:
                x_rand = randint(20, self.game_width - Constant.INFO_DISPLAY_SIZE)
                x_f = x_rand - x_rand % 20
                y_rand = randint(20, self.game_height)
                y_f = y_rand - y_rand % 20
                if [x_f, y_f] not in self.player.position and [x_f, y_f] not in self.food.position:
                    self.food.position.append([x_f, y_f])
                    break


class Player(object):

    def __init__(self, game):
        x = 0.45 * game.game_width
        y = 0.5 * game.game_height
        self.x = x - x % 20
        self.y = y - y % 20
        self.position = []
        self.position.append([self.x, self.y])
        self.food = 1
        self.eaten = False
        self.image = pygame.image.load("img/robotBody.png")
        self.x_change = 20
        self.y_change = 0
        self.time = 0

    def update_position(self, x, y):
        # if self.position[-1][0] != x or self.position[-1][1] != y:
        #     if self.food > 1:
        #         for i in range(0, self.food - 1):
        #             self.position[i][0], self.position[i][1] = self.position[i + 1]
        self.position[-1][0] = x
        self.position[-1][1] = y
        # pass

    def do_move(self, move, x, y, game, food, agent):
        move_array = [self.x_change, self.y_change]

        if self.eaten:
            # self.position.append([self.x, self.y])
            self.eaten = False
            self.food = self.food + 1

        if np.array_equal(move, [1, 0, 0, 0]):
            move_array = self.x_change, self.y_change
        elif np.array_equal(move, [0, 1, 0, 0]) and self.y_change == 0:  # right - going horizontal
            move_array = [0, self.x_change]
        elif np.array_equal(move, [0, 1, 0, 0]) and self.x_change == 0:  # right - going vertical
            move_array = [-self.y_change, 0]
        elif np.array_equal(move, [0, 0, 1, 0]) and self.y_change == 0:  # left - going horizontal
            move_array = [0, -self.x_change]
        elif np.array_equal(move, [0, 0, 1, 0]) and self.x_change == 0:  # left - going vertical
            move_array = [self.y_change, 0]
        elif np.array_equal(move, [0, 0, 0, 1]) and self.x_change == 0:  # left - going vertical
            move_array = [-self.x_change, -self.y_change]

        game.crash = False
        x_new = x + move_array[0]
        y_new = y + move_array[1]
        if x_new < 20 or x_new > game.game_width - Constant.INFO_DISPLAY_SIZE or \
                y_new < 20 or y_new > game.game_height or \
                [x_new, y_new] in self.position:
            game.crash = True
            return

        self.x_change, self.y_change = move_array
        self.x = x + self.x_change
        self.y = y + self.y_change

        if self.x < 20 or self.x > game.game_width - Constant.INFO_DISPLAY_SIZE or \
                self.y < 20 or self.y > game.game_height or \
                [self.x, self.y] in self.position:
            game.crash = True

        eat(self, food, game)

        self.update_position(self.x, self.y)
        # self.fuel -= 1
        self.time += 1

    def display_player(self, x, y, food, game):
        self.position[-1][0] = x
        self.position[-1][1] = y

        if len(game.food.position) > 0:
            x_temp, y_temp = self.position[0]
            game.gameDisplay.blit(self.image, (x_temp, y_temp))
            update_screen()
        else:
            pygame.time.wait(300)


class Food(object):

    def __init__(self):
        self.x_food = 0
        self.y_food = 0
        self.position = []
        self.image = pygame.image.load("img/food2.png")

    def reborn(self, game, player):
        x_rand = randint(20, game.game_width - Constant.INFO_DISPLAY_SIZE)
        self.x_food = x_rand - x_rand % 20
        y_rand = randint(20, game.game_height)
        self.y_food = y_rand - y_rand % 20
        if [self.x_food, self.y_food] not in player.position and [self.x_food, self.y_food] not in self.position:
            return self.x_food, self.y_food
        else:
            self.reborn(game, player)

    # def display_food(self, x, y, game):
    #     game.gameDisplay.blit(self.image, (x, y))
    #     update_screen()

    def display_food(self, game):
        for [x, y] in self.position:
            game.gameDisplay.blit(self.image, (x, y))
        update_screen()


def eat(player, food, game):
    # if player.x == food.x_food and player.y == food.y_food:
    #     food.reborn(game, player)
    #     player.eaten = True
    #     game.score = game.score + 1
    if [player.x, player.y] in food.position:
        food.position.remove([player.x, player.y])
        player.eaten = True
        game.score = game.score + 1


def get_record(time, record):
    if record <= 0 or time <= record:
        return time
    else:
        return record


def display_ui(game, score, record, time, game_count):
    myfont = pygame.font.SysFont('Segoe UI', 20)
    myfont_bold = pygame.font.SysFont('Segoe UI', 20, True)

    text_score = myfont.render('Garbages remaing: ', True, (0, 0, 0))
    text_score_number = myfont.render(str(Constant.NUMBER_OF_FOODS - score), True, (0, 0, 0))

    str_record = str(record)
    if record < 0:
        str_record = " "
    text_highest = myfont.render('Least fuel: ', True, (255, 0, 0))
    text_highest_number = myfont_bold.render(str_record, True, (255, 0, 0))

    text_time = myfont.render('Fuel: ', True, (0, 0, 255))
    text_time_number = myfont_bold.render(str(time), True, (0, 0, 255))

    text_count = myfont.render('Game count: ', True, (128, 0, 128))
    text_count_number = myfont_bold.render(str(game_count + 1) + " / " + str(Constant.PLAY_TIMES), True, (128, 0, 128))

    game.gameDisplay.blit(text_score, (Constant.GAME_WIDTH - Constant.INFO_DISPLAY_SIZE + 60, 50))
    game.gameDisplay.blit(text_score_number, (Constant.GAME_WIDTH - Constant.INFO_DISPLAY_SIZE + 240, 50))

    game.gameDisplay.blit(text_time, (Constant.GAME_WIDTH - Constant.INFO_DISPLAY_SIZE + 60, 100))
    game.gameDisplay.blit(text_time_number, (Constant.GAME_WIDTH - Constant.INFO_DISPLAY_SIZE + 240, 100))

    game.gameDisplay.blit(text_highest, (Constant.GAME_WIDTH - Constant.INFO_DISPLAY_SIZE + 60, 150))
    game.gameDisplay.blit(text_highest_number, (Constant.GAME_WIDTH - Constant.INFO_DISPLAY_SIZE + 240, 150))

    game.gameDisplay.blit(text_count, (Constant.GAME_WIDTH - Constant.INFO_DISPLAY_SIZE + 60, 300))
    game.gameDisplay.blit(text_count_number, (Constant.GAME_WIDTH - Constant.INFO_DISPLAY_SIZE + 240, 300))

    game.gameDisplay.blit(game.bg, (10, 10))


def display(player, food, game, record, game_count):
    game.gameDisplay.fill((255, 255, 255))
    display_ui(game, game.score, record, game.player.time, game_count)
    player.display_player(player.position[-1][0], player.position[-1][1], player.food, game)
    food.display_food(game)


def update_screen():
    pygame.display.update()


def initialize_game(player, game, food, agent):
    state_init1 = agent.get_state(game, player, food)  # [0 0 0 0 0 0 0 0 0 1 0 0 0 1 0 0]
    action = [1, 0, 0]
    player.do_move(action, player.x, player.y, game, food, agent)
    state_init2 = agent.get_state(game, player, food)
    reward1 = agent.set_reward(player, game)
    agent.remember(state_init1, action, reward1, state_init2, game.crash)
    agent.replay_new(agent.memory)


def plot_seaborn(array_counter, array_score):
    sns.set(color_codes=True)
    ax = sns.regplot(
        np.array([array_counter])[0],
        np.array([array_score])[0],
        color="r", x_jitter=.1,
        marker=".",
        scatter_kws={"s": 80},
        line_kws={'color': 'green'})
    ax.set(xlabel='Episodes', ylabel='Fuel used')
    plt.show()


def run():
    pygame.init()
    agent = DQNAgent()
    counter_games = 0
    score_plot = []
    counter_plot = []
    record = -1

    while counter_games < Constant.PLAY_TIMES:
        # Initialize classes
        game = Game(Constant.GAME_WIDTH, Constant.GAME_HEIGHT)
        game.create_foods()
        player1 = game.player
        food1 = game.food

        # Perform first move
        initialize_game(player1, game, food1, agent)
        if display_option:
            display(player1, food1, game, record, counter_games)

        while len(game.food.position) > 0:
            # agent.epsilon is set to give randomness to actions
            agent.epsilon = 1 / (exp(counter_games / 15) + 1 / 19)

            # get old state
            state_old = agent.get_state(game, player1, food1)

            # perform random actions based on agent.epsilon, or choose the action
            if random.uniform(0, 1) < agent.epsilon:
                # if randint(0, 200) < 80 - counter_games:
                final_move = to_categorical(randint(0, 3), num_classes=4)
            else:
                # predict action based on the old state
                prediction = agent.model.predict(state_old.reshape((1, Constant.INPUT_DIMENSIONS)))
                final_move = to_categorical(np.argmax(prediction[0]), num_classes=4)

            # perform new move and get new state
            player1.do_move(final_move, player1.x, player1.y, game, food1, agent)
            state_new = agent.get_state(game, player1, food1)

            # set treward for the new state
            reward = agent.set_reward(player1, game)

            # train short memory base on the new action and state
            agent.train_short_memory(state_old, final_move, reward, state_new, game.crash)

            # store the new data into a long term memory
            agent.remember(state_old, final_move, reward, state_new, game.crash)
            if display_option:
                display(player1, food1, game, record, counter_games)
                if counter_games in range(0, 20) or counter_games in range(90, 105):
                    pygame.time.wait(Constant.TIME_FRAME_DELAY)
                else:
                    pygame.time.wait(0)

        record = get_record(game.player.time, record)
        agent.replay_new(agent.memory)
        counter_games += 1
        print('Game', counter_games, '      Fuel used:', game.player.time)
        score_plot.append(game.player.time)
        counter_plot.append(counter_games)
    agent.model.save_weights('weights.hdf5')
    plot_seaborn(counter_plot, score_plot)


run()
