from keras.optimizers import Adam
from keras.models import Sequential
from keras.layers.core import Dense, Dropout
import random
import numpy as np
import pandas as pd
from operator import add

import Constant


def is_danger_straight(game, player):
    return (
                   player.x_change == 20 and player.y_change == 0 and  # Moving right
                   ((list(map(add, player.position[-1], [20, 0])) in player.position) or  # Hit itself
                    player.position[-1][0] + 20 >= (game.game_width - 20))) or (  # Hit wall

                   player.x_change == -20 and player.y_change == 0 and  # Moving left
                   ((list(map(add, player.position[-1], [-20, 0])) in player.position) or  # Hit itself
                    player.position[-1][0] - 20 < 20)) or (  # Hit wall

                   player.x_change == 0 and player.y_change == -20 and  # Moving up
                   ((list(map(add, player.position[-1], [0, -20])) in player.position) or  # Hit itslef
                    player.position[-1][-1] - 20 < 20)) or (  # Hit wall

                   player.x_change == 0 and player.y_change == 20 and  # Moving down
                   ((list(map(add, player.position[-1], [0, 20])) in player.position) or  # Hit itself
                    player.position[-1][-1] + 20 >= (game.game_height - 20)))  # Hit wall


def is_danger_right(game, player):
    return (
                   player.x_change == 0 and player.y_change == -20 and  # Moving up
                   ((list(map(add, player.position[-1], [20, 0])) in player.position) or  # Itself on the right
                    player.position[-1][0] + 20 > (game.game_width - 20))) or (  # Wall on the right

                   player.x_change == 0 and player.y_change == 20 and  # Moving down
                   ((list(map(add, player.position[-1], [-20, 0])) in player.position) or  # Itself on the right
                    player.position[-1][0] - 20 < 20)) or (  # Wall on the right

                   player.x_change == -20 and player.y_change == 0 and  # Moving left
                   ((list(map(add, player.position[-1], [0, -20])) in player.position) or  # Itself on the right
                    player.position[-1][-1] - 20 < 20)) or (  # Wall on the right

                   player.x_change == 20 and player.y_change == 0 and  # Moving right
                   ((list(map(add, player.position[-1], [0, 20])) in player.position) or  # Itself on the right
                    player.position[-1][-1] + 20 >= (game.game_height - 20))),  # Wall on the right


def is_danger_left(game, player):
    return (
                   player.x_change == 0 and player.y_change == 20 and  # Moving down
                   ((list(map(add, player.position[-1], [20, 0])) in player.position) or  # Itself on the left
                    player.position[-1][0] + 20 > (game.game_width - 20))) or (  # Wall on the left

                   player.x_change == 0 and player.y_change == -20 and  # Moving up
                   ((list(map(add, player.position[-1], [-20, 0])) in player.position) or  # Itself on the left
                    player.position[-1][0] - 20 < 20)) or (  # Wall on the left

                   player.x_change == 20 and player.y_change == 0 and  # Moving right
                   ((list(map(add, player.position[-1], [0, -20])) in player.position) or  # Itself on the left
                    player.position[-1][-1] - 20 < 20)) or (  # Wall on the left

                   player.x_change == -20 and player.y_change == 0 and  # Moving left
                   ((list(map(add, player.position[-1], [0, 20])) in player.position) or  # Itself on the left
                    player.position[-1][-1] + 20 >= (game.game_height - 20)))  # Wall on the left


def is_food_left(player, food):
    for [x, _] in food.position:
        if x < player.x:
            return True
    return False


def is_food_right(player, food):
    for [x, _] in food.position:
        if x > player.x:
            return True
    return False


def is_food_up(player, food):
    for [_, y] in food.position:
        if y < player.y:
            return True
    return False


def is_food_down(player, food):
    for [_, y] in food.position:
        if y > player.y:
            return True
    return False


def find_nearest_food(player, food):
    min_distance = Constant.GAME_HEIGHT * 2 * Constant.GAME_HEIGHT
    for [x, y] in food.position:
        dist = (x - player.x) * (x - player.x) + (y - player.y) * (y - player.y)
        if dist < min_distance:
            food.x_food = x
            food.y_food = y
            min_distance = dist


class DQNAgent(object):

    def __init__(self):
        self.reward = 0
        self.gamma = 0.9
        self.data_frame = pd.DataFrame()
        self.short_memory = np.array([])
        self.agent_target = 1
        self.agent_predict = 0
        self.learning_rate = Constant.LEARNING_RATE
        self.model = self.network()
        # self.model = self.network("weights.hdf5")
        self.epsilon = 0
        self.actual = []
        self.memory = []

    def get_state(self, game, player, food):

        find_nearest_food(player, food)
        state = [
            # player.fuel == 0,  # run out of fuel
            is_danger_straight(game, player),  # danger straight
            is_danger_right(game, player),  # danger right
            is_danger_left(game, player),  # danger left

            player.x_change == -20,  # move left
            player.x_change == 20,  # move right
            player.y_change == -20,  # move up
            player.y_change == 20,  # move down

            food.x_food < player.x,  # food left
            food.x_food > player.x,  # food right
            food.y_food < player.y,  # food up
            food.y_food > player.y  # food down
            # is_food_left(player, food),
            # is_food_right(player, food),
            # is_food_up(player, food),
            # is_food_down(player, food)
        ]

        for i in range(len(state)):
            if state[i]:
                state[i] = 1
            else:
                state[i] = 0

        return np.asarray(state)

    def set_reward(self, player, game):
        """ reward  = 0 if nothing happen
                    = 10 if the player eat the food
                    = -10 if the player run out of fuel
        """
        self.reward = 0
        # if player.fuel == 0:
        #     self.reward = -10
        #     return self.reward
        if player.eaten:
            self.reward = 10
        if game.crash:
            self.reward = -10
        return self.reward

    def network(self, weights=None):
        model = Sequential()
        model.add(Dense(250, activation='relu', input_dim=Constant.INPUT_DIMENSIONS))
        model.add(Dropout(0.15))
        model.add(Dense(250, activation='relu'))
        model.add(Dropout(0.15))
        model.add(Dense(250, activation='relu'))
        model.add(Dropout(0.4))
        model.add(Dense(4, activation='softmax'))
        opt = Adam(self.learning_rate)
        model.compile(loss='mse', optimizer=opt)

        if weights:
            model.load_weights(weights)
        return model

    def remember(self, state, action, reward, next_state, done):
        self.memory.append((state, action, reward, next_state, done))

    def replay_new(self, memory):
        if len(memory) > 1000:
            mini_batch = random.sample(memory, 1000)
        else:
            mini_batch = memory
        for state, action, reward, next_state, done in mini_batch:
            target = reward
            if not done:
                target = reward + self.gamma * np.amax(self.model.predict(np.array([next_state]))[0])
            target_f = self.model.predict(np.array([state]))
            target_f[0][np.argmax(action)] = target
            self.model.fit(np.array([state]), target_f, epochs=1, verbose=0)

    def train_short_memory(self, state, action, reward, next_state, done):
        target = reward
        if not done:
            target = reward + self.gamma * np.amax(
                self.model.predict(next_state.reshape((1, Constant.INPUT_DIMENSIONS)))[0])
        target_f = self.model.predict(state.reshape((1, Constant.INPUT_DIMENSIONS)))
        target_f[0][np.argmax(action)] = target
        self.model.fit(state.reshape((1, Constant.INPUT_DIMENSIONS)), target_f, epochs=1, verbose=0)
